<?php
/* THIS IS A GENERATED FILE. DO NOT EDIT DIRECTLY. */
$generated_i18n_strings = array(
	/* Copyright (C) 2020 AnalogWP
	   This file is distributed under the GPL2. */

	// Reference: src/js/app/Header.js:108
	// Reference: src/js/app/popup.js:134
	// Reference: languages/ang-translations.php:11
	__( 'Close', 'ang' ),

	// Reference: src/js/app/Header.js:93
	// Reference: languages/ang-translations.php:15
	__( 'Templates library refreshed', 'ang' ),

	// Reference: src/js/app/Header.js:94
	// Reference: languages/ang-translations.php:19
	__( 'Error refreshing templates library, please try again.', 'ang' ),

	// Reference: src/js/app/Header.js:98
	// Reference: languages/ang-translations.php:23
	__( 'Syncing...', 'ang' ),

	// Reference: src/js/app/Header.js:99
	// Reference: languages/ang-translations.php:27
	__( 'Sync Library', 'ang' ),

	// Reference: src/js/app/Nav.js:66
	// Reference: languages/ang-translations.php:31
	__( 'Templates', 'ang' ),

	// Reference: src/js/app/Nav.js:68
	// Reference: inc/elementor/class-ang-action.php:121
	// Reference: inc/elementor/class-post-type.php:32
	// Reference: inc/elementor/class-post-type.php:41
	// Reference: inc/elementor/class-typography.php:702
	// Reference: inc/elementor/kit/Kits_List_Table.php:309
	// Reference: inc/register-settings.php:27
	// Reference: inc/register-settings.php:56
	// Reference: languages/ang-translations.php:42
	__( 'Style Kits', 'ang' ),

	// Reference: src/js/app/Nav.js:69
	// Reference: languages/ang-translations.php:46
	__( 'Blocks', 'ang' ),

	// Reference: src/js/app/ProModal.js:37
	// Reference: languages/ang-translations.php:50
	__( 'To import Pro ', 'ang' ),

	// Reference: src/js/app/ProModal.js:37
	// Reference: languages/ang-translations.php:54
	__( ', you’ll need an active ', 'ang' ),

	// Reference: src/js/app/ProModal.js:37
	// Reference: inc/settings/class-settings-gopro.php:27
	// Reference: languages/ang-translations.php:59
	__( 'Style Kits Pro', 'ang' ),

	// Reference: src/js/app/ProModal.js:37
	// Reference: languages/ang-translations.php:63
	__( 'license.', 'ang' ),

	// Reference: src/js/app/ProModal.js:39
	// Reference: languages/ang-translations.php:67
	__( 'Learn More', 'ang' ),

	// Reference: src/js/app/Template.js:15
	// Reference: src/js/app/blocks/BlockList.js:223
	// Reference: languages/ang-translations.php:72
	__( 'New', 'ang' ),

	// Reference: src/js/app/Template.js:22
	// Reference: src/js/app/stylekits/stylekits.js:302
	// Reference: languages/ang-translations.php:77
	__( 'Preview', 'ang' ),

	// Reference: src/js/app/Template.js:25
	// Reference: src/js/app/blocks/BlockList.js:237
	// Reference: src/js/app/stylekits/stylekits.js:307
	// Reference: inc/elementor/Promotions.php:135
	// Reference: inc/Plugin.php:176
	// Reference: inc/register-settings.php:76
	// Reference: languages/ang-translations.php:88
	/* translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text.
translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: translators: translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. translators: translators: %1$s: Link to Style Kits Pro. %2$s: Go Pro text. */
	__( 'Go Pro', 'ang' ),

	// Reference: src/js/app/Template.js:29
	// Reference: src/js/app/blocks/BlockList.js:243
	// Reference: src/js/app/stylekits/stylekits.js:375
	// Reference: languages/ang-translations.php:94
	__( 'Import', 'ang' ),

	// Reference: src/js/app/Template.js:56
	// Reference: src/js/app/blocks/BlockList.js:261
	// Reference: src/js/app/stylekits/stylekits.js:327
	// Reference: languages/ang-translations.php:100
	__( 'Pro', 'ang' ),

	// Reference: src/js/app/Templates.js:311
	// Reference: languages/ang-translations.php:104
	__( 'This template requires an updated version, please update your plugin to latest version.', 'ang' ),

	// Reference: src/js/app/Templates.js:411
	// Reference: languages/ang-translations.php:108
	__( 'templates', 'ang' ),

	// Reference: src/js/app/api.js:205
	// Reference: languages/ang-translations.php:112
	__( 'Template', 'ang' ),

	// Reference: src/js/app/api.js:208
	// Reference: languages/ang-translations.php:116
	__( 'Block', 'ang' ),

	// Reference: src/js/app/api.js:216
	// Reference: languages/ang-translations.php:120
	__( 'Add Style Kits', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:140
	// Reference: src/js/app/filters.js:150
	// Reference: languages/ang-translations.php:125
	__( 'Show All', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:145
	// Reference: src/js/app/filters.js:155
	// Reference: languages/ang-translations.php:130
	__( 'Latest', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:146
	// Reference: src/js/app/filters.js:156
	// Reference: languages/ang-translations.php:135
	__( 'Popular', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:157
	// Reference: languages/ang-translations.php:139
	__( 'Back to all Blocks', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:173
	// Reference: src/js/app/filters.js:190
	// Reference: languages/ang-translations.php:144
	__( 'Back to all', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:174
	// Reference: src/js/app/filters.js:191
	// Reference: languages/ang-translations.php:149
	__( 'My Favorites', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:180
	// Reference: languages/ang-translations.php:153
	__( 'Show Pro Blocks', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:194
	// Reference: languages/ang-translations.php:157
	__( 'Group by Block type', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:210
	// Reference: src/js/app/filters.js:229
	// Reference: languages/ang-translations.php:162
	__( 'Filter', 'ang' ),

	// Reference: src/js/app/blocks/Filters.js:223
	// Reference: src/js/app/filters.js:244
	// Reference: languages/ang-translations.php:167
	__( 'Sort by', 'ang' ),

	// Reference: src/js/app/blocks/blocks.js:171
	// Reference: languages/ang-translations.php:171
	__( 'Loading blocks...', 'ang' ),

	// Reference: src/js/app/blocks/blocks.js:191
	// Reference: languages/ang-translations.php:175
	__( 'The block has been imported and is now available in the', 'ang' ),

	// Reference: src/js/app/blocks/blocks.js:202
	// Reference: languages/ang-translations.php:179
	__( 'Elementor section library', 'ang' ),

	// Reference: src/js/app/blocks/blocks.js:216
	// Reference: src/js/app/stylekits/stylekits.js:401
	// Reference: languages/ang-translations.php:184
	__( 'Ok, thanks', 'ang' ),

	// Reference: src/js/app/blocks/blocks.js:226
	// Reference: languages/ang-translations.php:188
	__( 'blocks', 'ang' ),

	// Reference: src/js/app/blocks/blocks.js:230
	// Reference: languages/ang-translations.php:192
	__( 'No blocks found.', 'ang' ),

	// Reference: src/js/app/collection/Collection.js:152
	// Reference: languages/ang-translations.php:196
	__( 'View Templates', 'ang' ),

	// Reference: src/js/app/filters.js:175
	// Reference: languages/ang-translations.php:200
	__( 'Back to Kits', 'ang' ),

	// Reference: src/js/app/filters.js:177
	// Reference: languages/ang-translations.php:204
	__( 'Template Kit', 'ang' ),

	// Reference: src/js/app/filters.js:197
	// Reference: languages/ang-translations.php:208
	__( 'Show Pro Templates', 'ang' ),

	// Reference: src/js/app/filters.js:211
	// Reference: languages/ang-translations.php:212
	__( 'Group by Template Kit', 'ang' ),

	// Reference: src/js/app/filters.js:256
	// Reference: languages/ang-translations.php:216
	__( 'Search templates', 'ang' ),

	// Reference: src/js/app/modal.js:106
	// Reference: src/js/app/modal/Preview.js:111
	// Reference: languages/ang-translations.php:221
	__( 'Insert Layout', 'ang' ),

	// Reference: src/js/app/modal.js:96
	// Reference: src/js/app/modal/Preview.js:100
	// Reference: languages/ang-translations.php:226
	__( 'Back to Library', 'ang' ),

	// Reference: src/js/app/modal.js:99
	// Reference: src/js/app/modal/Preview.js:103
	// Reference: languages/ang-translations.php:231
	__( 'Open Preview in New Tab', 'ang' ),

	// Reference: src/js/app/modal/Preview.js:118
	// Reference: languages/ang-translations.php:235
	__( 'Loading icon', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:102
	// Reference: languages/ang-translations.php:239
	__( 'Default', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:106
	// Reference: languages/ang-translations.php:243
	__( 'Installed', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:134
	// Reference: languages/ang-translations.php:247
	__( 'Learn more about this in %s.', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:137
	// Reference: languages/ang-translations.php:251
	__( 'Style Kits Docs', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:155
	// Reference: languages/ang-translations.php:255
	__( 'Choose a Style Kit...', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:172
	// Reference: languages/ang-translations.php:259
	__( 'Next', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:188
	// Reference: languages/ang-translations.php:263
	__( 'Change Style Kit', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:192
	// Reference: languages/ang-translations.php:267
	__( 'Import this template to your library to make it available in your Elementor ', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:193
	// Reference: languages/ang-translations.php:271
	__( 'Saved Templates', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:194
	// Reference: languages/ang-translations.php:275
	__( ' list for future use.', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:214
	// Reference: languages/ang-translations.php:279
	__( 'Import to Library', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:222
	// Reference: languages/ang-translations.php:283
	__( 'Create a new page from this template to make it available as a draft page in your Pages list.', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:226
	// Reference: languages/ang-translations.php:287
	__( 'Enter a Page Name', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:245
	// Reference: languages/ang-translations.php:291
	__( 'Import to page', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:257
	// Reference: languages/ang-translations.php:295
	__( 'All done! The template has been imported.', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:262
	// Reference: languages/ang-translations.php:299
	__( 'Edit Template', 'ang' ),

	// Reference: src/js/app/popups/ImportTemplate.js:80
	// Reference: languages/ang-translations.php:303
	__( 'Select a Style Kit to apply on this layout', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:171
	// Reference: languages/ang-translations.php:307
	__( '%s in WordPress dashboard.', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:174
	// Reference: languages/ang-translations.php:311
	__( 'Manage your Style Kits', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:264
	// Reference: languages/ang-translations.php:315
	__( 'These are some Style Kit presets that you can use as a starting point. Once you import a Style Kit, it will be added to your', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:264
	// Reference: languages/ang-translations.php:319
	__( 'Style Kits list', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:265
	// Reference: languages/ang-translations.php:323
	__( 'You will then be able to apply it on any Elementor page.', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:266
	// Reference: inc/elementor/class-ang-action.php:118
	// Reference: languages/ang-translations.php:330
	/* translators: translators: %s: Link text
translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text translators: translators: %s: Link text */
	__( 'Learn more', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:276
	// Reference: languages/ang-translations.php:334
	__( 'Import Style Kit', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:344
	// Reference: languages/ang-translations.php:338
	__( 'A Style Kit already exists with the same name. To import it again please enter a new name below:', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:348
	// Reference: languages/ang-translations.php:342
	__( 'Please try a different as a Style Kit with same name already exists.', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:352
	// Reference: languages/ang-translations.php:346
	__( 'Enter a Style Kit Name', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:387
	// Reference: languages/ang-translations.php:350
	__( 'The Style Kit has been imported and is now available in the list of the available Style Kits.', 'ang' ),

	// Reference: src/js/app/stylekits/stylekits.js:409
	// Reference: languages/ang-translations.php:354
	__( 'Importing ', 'ang' ),

	// Reference: inc/admin/class-admin.php:44
	// Reference: inc/class-tracker.php:131
	// Reference: inc/register-settings.php:26
	// Reference: languages/ang-translations.php:362
	/* translators: Plugin Name of the plugin
translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin translators: Plugin Name of the plugin */
	__( 'Style Kits for Elementor', 'ang' ),

	// Reference: languages/ang-translations.php:368
	/* translators: Plugin URI of the plugin
Author URI of the plugin
translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin translators: Plugin URI of the plugin Author URI of the plugin */
	__( 'https://analogwp.com/', 'ang' ),

	// Reference: languages/ang-translations.php:373
	/* translators: Description of the plugin
translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin */
	__( 'Style Kits extends the Elementor theme styles editor with more global styling options. Boost your design workflow in Elementor with intuitive global controls and theme style presets.', 'ang' ),

	// Reference: languages/ang-translations.php:378
	/* translators: Author of the plugin
translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin translators: Author of the plugin */
	__( 'AnalogWP', 'ang' ),

	// Reference: analogwp-templates.php:45
	// Reference: analogwp-templates.php:165
	// Reference: languages/ang-translations.php:385
	/* translators: translators: %s: version number
translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number translators: translators: %s: version number */
	__( 'Style Kit for Elementor requires PHP version %s', 'ang' ),

	// Reference: analogwp-templates.php:46
	// Reference: analogwp-templates.php:166
	// Reference: languages/ang-translations.php:390
	__( 'Error Activating', 'ang' ),

	// Reference: analogwp-templates.php:80
	// Reference: languages/ang-translations.php:396
	/* translators: translators: %s: WordPress version
translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: translators: translators: translators: translators: translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: translators: %s: WordPress version translators: translators: %s: WordPress version */
	__( 'Style Kits requires WordPress version %s+. Because you are using an earlier version, the plugin is currently NOT RUNNING.', 'ang' ),

	// Reference: analogwp-templates.php:103
	// Reference: languages/ang-translations.php:402
	/* translators: translators: %s: Minimum required Elementor version.
translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: %s: Minimum required Elementor version. translators: translators: translators: translators: %s: Minimum required Elementor version. */
	__( 'Style Kits requires Elementor v%s or newer in order to work. Please update Elementor to the latest version.', 'ang' ),

	// Reference: analogwp-templates.php:105
	// Reference: languages/ang-translations.php:406
	__( 'Update Elementor Now', 'ang' ),

	// Reference: analogwp-templates.php:139
	// Reference: languages/ang-translations.php:410
	__( 'Style Kits is not working because you need to activate the Elementor plugin.', 'ang' ),

	// Reference: analogwp-templates.php:140
	// Reference: languages/ang-translations.php:414
	__( 'Activate Elementor Now', 'ang' ),

	// Reference: analogwp-templates.php:147
	// Reference: languages/ang-translations.php:418
	__( 'Style Kits is not working because you need to install the Elementor plugin.', 'ang' ),

	// Reference: analogwp-templates.php:148
	// Reference: languages/ang-translations.php:422
	__( 'Install Elementor Now', 'ang' ),

	// Reference: inc/admin/class-admin.php:43
	// Reference: languages/ang-translations.php:428
	/* translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review
translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review translators: translators: 1: Style Kits for Elementor, 2: Link to plugin review */
	__( 'Enjoyed %1$s? Please leave us a %2$s rating. We really appreciate your support!', 'ang' ),

	// Reference: inc/admin/class-admin.php:65
	// Reference: languages/ang-translations.php:432
	__( 'View Documentation', 'ang' ),

	// Reference: inc/admin/class-admin.php:65
	// Reference: inc/settings/views/html-admin-settings.php:61
	// Reference: languages/ang-translations.php:437
	__( 'Documentation', 'ang' ),

	// Reference: inc/admin/class-admin.php:66
	// Reference: languages/ang-translations.php:441
	__( 'Get Support', 'ang' ),

	// Reference: inc/admin/Notice.php:172
	// Reference: languages/ang-translations.php:445
	__( 'Style Kits for Elementor Logo', 'ang' ),

	// Reference: inc/api/class-local.php:127
	// Reference: inc/api/class-local.php:320
	// Reference: languages/ang-translations.php:450
	__( 'Invalid or expired license provided.', 'ang' ),

	// Reference: inc/api/class-local.php:393
	// Reference: languages/ang-translations.php:454
	__( 'Setting updated.', 'ang' ),

	// Reference: inc/api/class-local.php:452
	// Reference: languages/ang-translations.php:458
	__( 'Invalid param(s).', 'ang' ),

	// Reference: inc/api/class-local.php:456
	// Reference: languages/ang-translations.php:462
	__( 'Please provide a title.', 'ang' ),

	// Reference: inc/api/class-local.php:473
	// Reference: languages/ang-translations.php:466
	__( 'Unable to create a Kit', 'ang' ),

	// Reference: inc/api/class-local.php:479
	// Reference: languages/ang-translations.php:470
	__( 'The new Theme Style Kit has been saved and applied on this page.', 'ang' ),

	// Reference: inc/api/class-local.php:496
	// Reference: inc/api/class-local.php:526
	// Reference: languages/ang-translations.php:475
	__( 'Please provide a valid post ID.', 'ang' ),

	// Reference: inc/api/class-local.php:500
	// Reference: languages/ang-translations.php:479
	__( 'Invalid Post ID', 'ang' ),

	// Reference: inc/api/class-local.php:553
	// Reference: languages/ang-translations.php:483
	__( 'Invalid Style Kit ID.', 'ang' ),

	// Reference: inc/api/class-local.php:583
	// Reference: languages/ang-translations.php:487
	__( 'Error occured while requesting Style Kit data.', 'ang' ),

	// Reference: inc/api/class-local.php:613
	// Reference: languages/ang-translations.php:491
	__( 'Style Kit imported', 'ang' ),

	// Reference: inc/api/class-local.php:648
	// Reference: languages/ang-translations.php:495
	__( 'Invalid token data returned', 'ang' ),

	// Reference: inc/api/class-local.php:666
	// Reference: languages/ang-translations.php:499
	__( 'Invalid Block ID.', 'ang' ),

	// Reference: inc/class-admin-settings.php:76
	// Reference: languages/ang-translations.php:503
	__( 'Your settings have been saved.', 'ang' ),

	// Reference: inc/class-admin-settings.php:133
	// Reference: languages/ang-translations.php:507
	__( 'The changes you made will be lost if you navigate away from this page.', 'ang' ),

	// Reference: inc/class-admin-settings.php:550
	// Reference: inc/class-admin-settings.php:615
	// Reference: languages/ang-translations.php:512
	__( 'Toggle', 'ang' ),

	// Reference: inc/class-base.php:39
	// Reference: inc/class-base.php:49
	// Reference: languages/ang-translations.php:517
	__( 'Something went wrong.', 'ang' ),

	// Reference: inc/class-cron.php:41
	// Reference: languages/ang-translations.php:521
	__( 'Once Weekly', 'ang' ),

	// Reference: inc/class-elementor.php:46
	// Reference: languages/ang-translations.php:525
	__( 'AnalogWP Classes', 'ang' ),

	// Reference: inc/class-quick-edit.php:83
	// Reference: inc/class-quick-edit.php:137
	// Reference: inc/elementor/class-post-type.php:33
	// Reference: languages/ang-translations.php:531
	__( 'Style Kit', 'ang' ),

	// Reference: inc/class-tracker.php:130
	// Reference: languages/ang-translations.php:537
	/* translators: translators: %2$s Plugin Name %3%s Review text
translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text translators: translators: %2$s Plugin Name %3%s Review text */
	__( 'Hey! You have been using %1$s for over 2 weeks, we hope you enjoy it! If so, please leave a positive %2$s.', 'ang' ),

	// Reference: inc/class-tracker.php:132
	// Reference: languages/ang-translations.php:541
	__( 'review on WordPress.org', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:100
	// Reference: languages/ang-translations.php:545
	__( 'This will clean-up all the values from the current Theme Style kit. If you need to revert, you can do so at the Revisions tab.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:101
	// Reference: languages/ang-translations.php:549
	__( 'Are you sure?', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:102
	// Reference: languages/ang-translations.php:553
	__( 'Save Style Kit as', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:103
	// Reference: languages/ang-translations.php:557
	__( 'Save', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:104
	// Reference: languages/ang-translations.php:561
	__( 'Cancel', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:105
	// Reference: languages/ang-translations.php:565
	__( 'Enter a title', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:106
	// Reference: languages/ang-translations.php:569
	__( 'Insert Style Kit', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:107
	// Reference: inc/elementor/class-ang-action.php:110
	// Reference: languages/ang-translations.php:574
	__( 'Please select a Style Kit first.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:108
	// Reference: languages/ang-translations.php:578
	__( '— Select a Style Kit —', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:109
	// Reference: languages/ang-translations.php:582
	__( 'Style Kit Updated.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:111
	// Reference: languages/ang-translations.php:586
	__( 'Update Style Kit', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:112
	// Reference: languages/ang-translations.php:590
	__( 'This action will update the Style Kit with the latest changes, and will affect all the pages that the style kit is used on. Do you wish to proceed?', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:113
	// Reference: languages/ang-translations.php:594
	__( 'Meet Style Kits for Elementor', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:116
	// Reference: languages/ang-translations.php:600
	/* translators: translators: %s: Link to Style Kits documentation.
translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. translators: translators: %s: Link to Style Kits documentation. */
	__( 'Take control of your design in the macro level, with local or global settings for typography and spacing. %s.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:120
	// Reference: languages/ang-translations.php:604
	__( 'View Styles', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:122
	// Reference: languages/ang-translations.php:608
	__( 'Export CSS', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:123
	// Reference: languages/ang-translations.php:612
	__( 'Copy CSS', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:124
	// Reference: languages/ang-translations.php:616
	__( 'Style Kit Update Detected', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:125
	// Reference: languages/ang-translations.php:620
	__( '<p>The Style kit used by this page has been updated, click ‘Apply Changes’ to apply the latest changes.</p><p>Click Discard to keep your current page styles and detach the page from the Style Kit</p>', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:126
	// Reference: languages/ang-translations.php:624
	__( 'Discard', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:127
	// Reference: languages/ang-translations.php:628
	__( 'Apply Changes', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:128
	// Reference: languages/ang-translations.php:632
	__( 'Ok, got it.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:129
	// Reference: languages/ang-translations.php:636
	__( 'Go to Page Style', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:130
	// Reference: languages/ang-translations.php:640
	__( 'This template offers global typography and spacing control, through the Page Style tab.', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:131
	// Reference: languages/ang-translations.php:644
	__( 'Typography, column gaps and more, are controlled layout-wide at Page Styles Panel, giving you the flexibility you need over the design of this template. You can save the styles and apply them to any other page. <a href="#" target="_blank">Learn More.</a>', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:132
	// Reference: languages/ang-translations.php:648
	__( 'CSS Variables', 'ang' ),

	// Reference: inc/elementor/class-ang-action.php:133
	// Reference: languages/ang-translations.php:652
	__( 'Remove Page ID from the CSS', 'ang' ),

	// Reference: inc/elementor/class-colors.php:162
	// Reference: languages/ang-translations.php:658
	/* translators: translators: %1$s: Link to documentation, %2$s: Link text.
translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: translators: translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. */
	__( 'Set the accent colors of your layout.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:162
	// Reference: inc/elementor/class-typography.php:260
	// Reference: inc/elementor/class-typography.php:309
	// Reference: inc/elementor/class-typography.php:391
	// Reference: inc/elementor/class-typography.php:445
	// Reference: inc/elementor/class-typography.php:500
	// Reference: languages/ang-translations.php:669
	/* translators: translators: %1$s: Link to documentation, %2$s: Link text.
translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. */
	__( 'Learn more.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:232
	// Reference: languages/ang-translations.php:673
	__( 'Primary Accent', 'ang' ),

	// Reference: inc/elementor/class-colors.php:245
	// Reference: languages/ang-translations.php:679
	/* translators: translators: %s: Typography Panel link/text.
translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: translators: translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. translators: translators: %s: Typography Panel link/text. */
	__( 'The primary accent color applies on links, icons, and other elements. You can also define the text link color in the %s.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:246
	// Reference: languages/ang-translations.php:683
	__( 'Typography panel', 'ang' ),

	// Reference: inc/elementor/class-colors.php:267
	// Reference: languages/ang-translations.php:687
	__( 'Secondary Accent', 'ang' ),

	// Reference: inc/elementor/class-colors.php:286
	// Reference: languages/ang-translations.php:693
	/* translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text.
translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: translators: translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. */
	__( 'The default button color. You can also define button colors under the %1$s, and individually for each button size under %2$s.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:287
	// Reference: languages/ang-translations.php:697
	__( 'Buttons panel', 'ang' ),

	// Reference: inc/elementor/class-colors.php:288
	// Reference: languages/ang-translations.php:701
	__( 'Buttons Sizes panel', 'ang' ),

	// Reference: inc/elementor/class-finder-shortcuts.php:23
	// Reference: languages/ang-translations.php:705
	__( 'Style Kits for Elementor Shortcuts', 'ang' ),

	// Reference: inc/elementor/class-finder-shortcuts.php:36
	// Reference: languages/ang-translations.php:709
	__( 'Templates Library', 'ang' ),

	// Reference: inc/elementor/class-finder-shortcuts.php:42
	// Reference: inc/elementor/kit/Manager.php:296
	// Reference: inc/Plugin.php:170
	// Reference: inc/register-settings.php:46
	// Reference: languages/ang-translations.php:716
	__( 'Settings', 'ang' ),

	// Reference: inc/elementor/class-finder-shortcuts.php:48
	// Reference: inc/register-settings.php:57
	// Reference: languages/ang-translations.php:721
	__( 'Theme Style Kits', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:37
	// Reference: languages/ang-translations.php:725
	__( 'Add New Style Kit', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:38
	// Reference: languages/ang-translations.php:729
	__( 'New Style Kit', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:39
	// Reference: languages/ang-translations.php:733
	__( 'Edit Style Kit', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:40
	// Reference: languages/ang-translations.php:737
	__( 'View Style Kit', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:42
	// Reference: languages/ang-translations.php:741
	__( 'Search Style Kits', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:43
	// Reference: languages/ang-translations.php:745
	__( 'Parent Style Kits:', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:44
	// Reference: languages/ang-translations.php:749
	__( 'No Style Kit found.', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:45
	// Reference: languages/ang-translations.php:753
	__( 'No Style Kit found in Trash.', 'ang' ),

	// Reference: inc/elementor/class-tools.php:117
	// Reference: languages/ang-translations.php:757
	__( 'Error occurred, the version selected is invalid. Try selecting different version.', 'ang' ),

	// Reference: inc/elementor/class-tools.php:162
	// Reference: languages/ang-translations.php:761
	__( 'Rollback to Previous Version', 'ang' ),

	// Reference: inc/elementor/class-tools.php:226
	// Reference: languages/ang-translations.php:767
	/* translators: translators: %s: Style kit title.
translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. translators: translators: %s: Style kit title. */
	__( 'Style Kit: %s <span style="color:#5C32B6;">&#9679;</span>', 'ang' ),

	// Reference: inc/elementor/class-tools.php:260
	// Reference: languages/ang-translations.php:771
	__( 'Apply Global Style Kit', 'ang' ),

	// Reference: inc/elementor/class-tools.php:306
	// Reference: languages/ang-translations.php:775
	__( 'Invalid/empty Post ID.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:139
	// Reference: languages/ang-translations.php:779
	__( 'Headings Typography', 'ang' ),

	// Reference: inc/elementor/class-typography.php:147
	// Reference: languages/ang-translations.php:783
	__( 'These settings apply to all Headings in your layout. You can still override individual values at each element.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:171
	// Reference: languages/ang-translations.php:789
	/* translators: translators: %s: Heading 1-6 type
translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type translators: translators: %s: Heading 1-6 type */
	__( 'Heading %s', 'ang' ),

	// Reference: inc/elementor/class-typography.php:195
	// Reference: inc/elementor/class-typography.php:217
	// Reference: languages/ang-translations.php:794
	__( 'Body Typography', 'ang' ),

	// Reference: inc/elementor/class-typography.php:203
	// Reference: languages/ang-translations.php:798
	__( 'Recently Imported', 'ang' ),

	// Reference: inc/elementor/class-typography.php:236
	// Reference: languages/ang-translations.php:802
	__( 'Typographic Sizes', 'ang' ),

	// Reference: inc/elementor/class-typography.php:253
	// Reference: languages/ang-translations.php:806
	__( 'Heading Sizes', 'ang' ),

	// Reference: inc/elementor/class-typography.php:260
	// Reference: languages/ang-translations.php:810
	__( 'Edit the available sizes for the Heading Element.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:267
	// Reference: inc/elementor/class-typography.php:316
	// Reference: languages/ang-translations.php:815
	__( 'XXL', 'ang' ),

	// Reference: inc/elementor/class-typography.php:268
	// Reference: inc/elementor/class-typography.php:317
	// Reference: inc/elementor/class-typography.php:511
	// Reference: languages/ang-translations.php:821
	__( 'XL', 'ang' ),

	// Reference: inc/elementor/class-typography.php:269
	// Reference: inc/elementor/class-typography.php:318
	// Reference: inc/elementor/class-typography.php:354
	// Reference: inc/elementor/class-typography.php:382
	// Reference: inc/elementor/class-typography.php:861
	// Reference: languages/ang-translations.php:829
	__( 'Large', 'ang' ),

	// Reference: inc/elementor/class-typography.php:270
	// Reference: inc/elementor/class-typography.php:319
	// Reference: inc/elementor/class-typography.php:353
	// Reference: inc/elementor/class-typography.php:381
	// Reference: inc/elementor/class-typography.php:860
	// Reference: languages/ang-translations.php:837
	__( 'Medium', 'ang' ),

	// Reference: inc/elementor/class-typography.php:271
	// Reference: inc/elementor/class-typography.php:320
	// Reference: inc/elementor/class-typography.php:352
	// Reference: inc/elementor/class-typography.php:380
	// Reference: inc/elementor/class-typography.php:859
	// Reference: languages/ang-translations.php:845
	__( 'Small', 'ang' ),

	// Reference: inc/elementor/class-typography.php:289
	// Reference: languages/ang-translations.php:849
	__( 'Heading', 'ang' ),

	// Reference: inc/elementor/class-typography.php:302
	// Reference: languages/ang-translations.php:853
	__( 'Text Sizes', 'ang' ),

	// Reference: inc/elementor/class-typography.php:309
	// Reference: languages/ang-translations.php:857
	__( 'Edit the available sizes for the p, span, and div tags of the Heading Element.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:328
	// Reference: languages/ang-translations.php:861
	__( 'Text', 'ang' ),

	// Reference: inc/elementor/class-typography.php:351
	// Reference: inc/elementor/class-typography.php:379
	// Reference: inc/elementor/class-typography.php:858
	// Reference: languages/ang-translations.php:867
	__( 'Normal', 'ang' ),

	// Reference: inc/elementor/class-typography.php:355
	// Reference: inc/elementor/class-typography.php:383
	// Reference: inc/elementor/class-typography.php:862
	// Reference: languages/ang-translations.php:873
	__( 'Extra Large', 'ang' ),

	// Reference: inc/elementor/class-typography.php:361
	// Reference: inc/elementor/class-typography.php:851
	// Reference: languages/ang-translations.php:878
	__( 'Outer Section Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:374
	// Reference: languages/ang-translations.php:882
	__( 'Set a Default Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:378
	// Reference: inc/elementor/class-typography.php:857
	// Reference: languages/ang-translations.php:887
	__( 'No Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:391
	// Reference: languages/ang-translations.php:891
	__( 'Add padding to the outer sections of your layouts by using these controls.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:427
	// Reference: languages/ang-translations.php:895
	__( 'Default Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:428
	// Reference: languages/ang-translations.php:899
	__( 'Narrow Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:429
	// Reference: languages/ang-translations.php:903
	__( 'Extended Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:430
	// Reference: languages/ang-translations.php:907
	__( 'Wide Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:431
	// Reference: languages/ang-translations.php:911
	__( 'Wider Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:437
	// Reference: languages/ang-translations.php:915
	__( 'Column Gaps', 'ang' ),

	// Reference: inc/elementor/class-typography.php:445
	// Reference: languages/ang-translations.php:919
	__( 'Column Gap presets add padding to the columns of a section.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:469
	// Reference: languages/ang-translations.php:923
	__( 'Space Between Widgets', 'ang' ),

	// Reference: inc/elementor/class-typography.php:470
	// Reference: languages/ang-translations.php:927
	__( 'Sets the default space between widgets, overrides the default value set in Elementor > Style > Space Between Widgets.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:492
	// Reference: languages/ang-translations.php:931
	__( 'Button Sizes', 'ang' ),

	// Reference: inc/elementor/class-typography.php:500
	// Reference: languages/ang-translations.php:935
	__( 'Define the default styles for every button size.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:507
	// Reference: languages/ang-translations.php:939
	__( 'XS', 'ang' ),

	// Reference: inc/elementor/class-typography.php:508
	// Reference: languages/ang-translations.php:943
	__( 'S', 'ang' ),

	// Reference: inc/elementor/class-typography.php:509
	// Reference: languages/ang-translations.php:947
	__( 'M', 'ang' ),

	// Reference: inc/elementor/class-typography.php:510
	// Reference: languages/ang-translations.php:951
	__( 'L', 'ang' ),

	// Reference: inc/elementor/class-typography.php:523
	// Reference: languages/ang-translations.php:955
	__( 'Typography', 'ang' ),

	// Reference: inc/elementor/class-typography.php:540
	// Reference: languages/ang-translations.php:959
	__( 'Normal Styling', 'ang' ),

	// Reference: inc/elementor/class-typography.php:548
	// Reference: inc/elementor/class-typography.php:607
	// Reference: inc/elementor/sections/background-color-classes.php:89
	// Reference: inc/elementor/sections/background-color-classes.php:175
	// Reference: languages/ang-translations.php:966
	__( 'Text Color', 'ang' ),

	// Reference: inc/elementor/class-typography.php:559
	// Reference: inc/elementor/class-typography.php:618
	// Reference: inc/elementor/sections/background-color-classes.php:75
	// Reference: inc/elementor/sections/background-color-classes.php:161
	// Reference: languages/ang-translations.php:973
	__( 'Background Color', 'ang' ),

	// Reference: inc/elementor/class-typography.php:586
	// Reference: inc/elementor/class-typography.php:645
	// Reference: languages/ang-translations.php:978
	__( 'Border Radius', 'ang' ),

	// Reference: inc/elementor/class-typography.php:599
	// Reference: languages/ang-translations.php:982
	__( 'Hover Styling', 'ang' ),

	// Reference: inc/elementor/class-typography.php:658
	// Reference: languages/ang-translations.php:986
	__( 'Padding', 'ang' ),

	// Reference: inc/elementor/class-typography.php:722
	// Reference: languages/ang-translations.php:990
	__( 'You are editing the Global Style Kit.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:731
	// Reference: languages/ang-translations.php:994
	__( 'A Style Kit is a saved configuration of Theme Styles, that you can optionally apply on any page. This will override the Global theme Styles for this page.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:735
	// Reference: languages/ang-translations.php:998
	__( 'Page Style Kit', 'ang' ),

	// Reference: inc/elementor/class-typography.php:749
	// Reference: inc/elementor/class-typography.php:751
	// Reference: languages/ang-translations.php:1003
	__( 'Edit Theme Style Kit', 'ang' ),

	// Reference: inc/elementor/class-typography.php:762
	// Reference: languages/ang-translations.php:1009
	/* translators: translators: %s: Link to Style Kits
translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits translators: translators: %s: Link to Style Kits */
	__( 'You can set a Global Style Kit <a href="%s" target="_blank">here</a>.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:784
	// Reference: languages/ang-translations.php:1013
	__( 'Theme Style Kit', 'ang' ),

	// Reference: inc/elementor/class-typography.php:789
	// Reference: languages/ang-translations.php:1017
	__( 'This will reset the Theme Style Kit and clean up any values.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:793
	// Reference: languages/ang-translations.php:1021
	__( 'Reset Theme Style Kit', 'ang' ),

	// Reference: inc/elementor/class-typography.php:795
	// Reference: languages/ang-translations.php:1025
	__( 'Reset', 'ang' ),

	// Reference: inc/elementor/class-typography.php:800
	// Reference: languages/ang-translations.php:1029
	__( 'Save the current styles as a different Theme Style Kit. You can then apply it on other pages, or globally.', 'ang' ),

	// Reference: inc/elementor/class-typography.php:804
	// Reference: languages/ang-translations.php:1033
	__( 'Save Theme Style Kit as', 'ang' ),

	// Reference: inc/elementor/class-typography.php:806
	// Reference: languages/ang-translations.php:1037
	__( 'Save as&hellip;', 'ang' ),

	// Reference: inc/elementor/class-typography.php:814
	// Reference: languages/ang-translations.php:1041
	__( 'Export Theme Style Kit CSS', 'ang' ),

	// Reference: inc/elementor/class-typography.php:816
	// Reference: languages/ang-translations.php:1045
	__( 'Export', 'ang' ),

	// Reference: inc/elementor/class-typography.php:852
	// Reference: languages/ang-translations.php:1049
	__( 'A Style Kits control that adds padding to your outer sections. You can edit the values', 'ang' ),

	// Reference: inc/elementor/class-typography.php:1082
	// Reference: languages/ang-translations.php:1053
	__( 'Headings', 'ang' ),

	// Reference: inc/elementor/class-typography.php:1091
	// Reference: inc/elementor/sections/background-color-classes.php:132
	// Reference: inc/elementor/sections/background-color-classes.php:218
	// Reference: languages/ang-translations.php:1059
	__( 'Headings Color', 'ang' ),

	// Reference: inc/elementor/class-typography.php:1104
	// Reference: languages/ang-translations.php:1063
	__( 'Headings Font', 'ang' ),

	// Reference: inc/elementor/class-typography.php:1115
	// Reference: languages/ang-translations.php:1067
	__( 'You can set individual heading font and colors below.', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:63
	// Reference: languages/ang-translations.php:1071
	__( 'No Kits found.', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:83
	// Reference: languages/ang-translations.php:1077
	/* translators: translators: %s: Human-readable time difference.
translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. translators: translators: %s: Human-readable time difference. */
	__( '%s ago', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:88
	// Reference: languages/ang-translations.php:1081
	__( 'Published', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:99
	// Reference: languages/ang-translations.php:1085
	__( 'Entire Site', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:101
	// Reference: languages/ang-translations.php:1089
	__( 'None', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:118
	// Reference: languages/ang-translations.php:1093
	__( 'Title', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:119
	// Reference: languages/ang-translations.php:1097
	__( 'Instances', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:120
	// Reference: languages/ang-translations.php:1101
	__( 'Author', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:121
	// Reference: languages/ang-translations.php:1105
	__( 'Date', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:138
	// Reference: languages/ang-translations.php:1111
	/* translators: translators: %s: Kit Title
translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: translators: translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title translators: translators: %s: Kit Title */
	__( '%s (Edit)', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:140
	// Reference: languages/ang-translations.php:1115
	__( 'Global Style Kit', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:147
	// Reference: languages/ang-translations.php:1119
	__( 'Edit', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:148
	// Reference: languages/ang-translations.php:1123
	__( 'Trash', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:149
	// Reference: languages/ang-translations.php:1127
	__( 'Export Theme Style Kit', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:150
	// Reference: languages/ang-translations.php:1131
	__( 'Edit with Elementor', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:247
	// Reference: languages/ang-translations.php:1135
	__( 'Move to Trash', 'ang' ),

	// Reference: inc/elementor/kit/Manager.php:294
	// Reference: languages/ang-translations.php:1141
	/* translators: translators: %1$s: Settings library link. %2$s: Settings page link.
translators: translators: %1$s: Settings library link. %2$s: Settings page link. translators: translators: %1$s: Settings library link. %2$s: Settings page link. */
	__( 'Here is a quick overview of your available Theme Style Kits. They are also available in the %1$s. You can define a Global Theme Style Kit in %2$s.', 'ang' ),

	// Reference: inc/elementor/kit/Manager.php:295
	// Reference: languages/ang-translations.php:1145
	__( 'Elementor Template Library', 'ang' ),

	// Reference: inc/elementor/Promotions.php:59
	// Reference: languages/ang-translations.php:1149
	__( 'Layout Tools', 'ang' ),

	// Reference: inc/elementor/Promotions.php:61
	// Reference: languages/ang-translations.php:1153
	__( 'Handy tools to clean inline styles from your existing layouts and make them global-design-ready. Highlight elements that have Classes or custom CSS.', 'ang' ),

	// Reference: inc/elementor/Promotions.php:96
	// Reference: languages/ang-translations.php:1157
	__( 'Advanced Form Controls', 'ang' ),

	// Reference: inc/elementor/Promotions.php:98
	// Reference: languages/ang-translations.php:1161
	__( 'Offers controls to customize form column/rows gap, label spacing, and form messages colors.', 'ang' ),

	// Reference: inc/elementor/sections/background-color-classes.php:60
	// Reference: languages/ang-translations.php:1165
	__( 'Light', 'ang' ),

	// Reference: inc/elementor/sections/background-color-classes.php:67
	// Reference: languages/ang-translations.php:1169
	__( 'Add the class <strong>sk-light-bg</strong> to a section or column to apply these colors.', 'ang' ),

	// Reference: inc/elementor/sections/background-color-classes.php:146
	// Reference: languages/ang-translations.php:1173
	__( 'Dark', 'ang' ),

	// Reference: inc/elementor/sections/background-color-classes.php:153
	// Reference: languages/ang-translations.php:1177
	__( 'Add the class <strong>sk-dark-bg</strong> to a section or column to apply these colors.', 'ang' ),

	// Reference: inc/elementor/tags/class-dark-background.php:14
	// Reference: languages/ang-translations.php:1181
	__( 'Dark Background', 'ang' ),

	// Reference: inc/elementor/tags/class-light-background.php:14
	// Reference: languages/ang-translations.php:1185
	__( 'Light Background', 'ang' ),

	// Reference: inc/register-settings.php:37
	// Reference: languages/ang-translations.php:1189
	__( 'Style Kits Library', 'ang' ),

	// Reference: inc/register-settings.php:38
	// Reference: languages/ang-translations.php:1193
	__( 'Library', 'ang' ),

	// Reference: inc/register-settings.php:45
	// Reference: inc/settings/views/html-admin-settings.php:26
	// Reference: languages/ang-translations.php:1198
	__( 'Style Kits Settings', 'ang' ),

	// Reference: inc/register-settings.php:65
	// Reference: inc/register-settings.php:66
	// Reference: languages/ang-translations.php:1203
	__( 'Welcome to Style Kits', 'ang' ),

	// Reference: inc/register-settings.php:253
	// Reference: languages/ang-translations.php:1207
	__( 'Style Kits are now integrated into Elementor Theme Styles', 'ang' ),

	// Reference: inc/register-settings.php:256
	// Reference: languages/ang-translations.php:1211
	__( 'With the introduction of Theme Styles in Elementor v2.9.0, 	Style Kits are now <strong>integrated into the Theme Styles panel</strong>, bringing you a consistent, native experience.', 'ang' ),

	// Reference: inc/register-settings.php:261
	// Reference: languages/ang-translations.php:1215
	__( 'See what’s different in this quick video above.', 'ang' ),

	// Reference: inc/register-settings.php:265
	// Reference: languages/ang-translations.php:1219
	__( 'Need help?', 'ang' ),

	// Reference: inc/register-settings.php:267
	// Reference: languages/ang-translations.php:1223
	__( 'Go to docs', 'ang' ),

	// Reference: inc/register-settings.php:268
	// Reference: languages/ang-translations.php:1227
	__( 'Send a support ticket', 'ang' ),

	// Reference: inc/settings/class-settings-extensions.php:25
	// Reference: languages/ang-translations.php:1231
	__( 'Extensions', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:26
	// Reference: inc/settings/class-settings-general.php:38
	// Reference: languages/ang-translations.php:1236
	__( 'General', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:61
	// Reference: languages/ang-translations.php:1240
	__( 'Elementor Settings', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:69
	// Reference: languages/ang-translations.php:1246
	/* translators: translators: %s: Style Kit Documentation link
translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link */
	__( 'Choosing a Style Kit will make it global and apply site-wide. Learn more about %s.', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:70
	// Reference: languages/ang-translations.php:1250
	__( 'Style kits', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:23
	// Reference: languages/ang-translations.php:1254
	__( 'Misc', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:42
	// Reference: languages/ang-translations.php:1258
	__( 'Usage Data Tracking', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:43
	// Reference: languages/ang-translations.php:1262
	__( 'Opt-in to our anonymous plugin data collection and to updates', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:48
	// Reference: languages/ang-translations.php:1266
	__( 'We guarantee no sensitive data is collected. ', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:48
	// Reference: languages/ang-translations.php:1270
	__( 'More Info', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:51
	// Reference: languages/ang-translations.php:1274
	__( 'Remove Data on Uninstall', 'ang' ),

	// Reference: inc/settings/class-settings-misc.php:52
	// Reference: languages/ang-translations.php:1278
	__( 'Check this box to remove all data stored by Style Kit for Elementor plugin, including license info, user settings, import history etc. Any imported or manually saved Style Kits are not removed.', 'ang' ),

	// Reference: inc/settings/class-settings-version-control.php:25
	// Reference: languages/ang-translations.php:1282
	__( 'Version Control', 'ang' ),

	// Reference: inc/settings/class-settings-version-control.php:40
	// Reference: languages/ang-translations.php:1286
	__( 'Rollback Versions', 'ang' ),

	// Reference: inc/settings/class-settings-version-control.php:41
	// Reference: languages/ang-translations.php:1290
	__( 'If you are having issues with current version of Style Kits for Elementor, you can rollback to a previous stable version.', 'ang' ),

	// Reference: inc/settings/class-settings-version-control.php:46
	// Reference: languages/ang-translations.php:1294
	__( 'Rollback Style Kits', 'ang' ),

	// Reference: inc/settings/class-settings-version-control.php:58
	// Reference: languages/ang-translations.php:1298
	__( 'Reinstall this version', 'ang' ),

	// Reference: inc/settings/class-settings-version-control.php:65
	// Reference: languages/ang-translations.php:1302
	__( 'Beta Features', 'ang' ),

	// Reference: inc/settings/class-settings-version-control.php:70
	// Reference: languages/ang-translations.php:1306
	__( 'Become a beta tester', 'ang' ),

	// Reference: inc/settings/class-settings-version-control.php:71
	// Reference: languages/ang-translations.php:1310
	__( 'Check this box to turn on beta updates for Style Kits and Style Kits Pro. The update will not be installed automatically, you always have the option to ignore it.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:14
	// Reference: languages/ang-translations.php:1314
	__( 'Style Kits Pro gives you access to <u>premium template kits, blocks and Style Kits</u>, <br>plus a number of extra design features to power-up your design workflow in Elementor.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:16
	// Reference: languages/ang-translations.php:1318
	__( '<strong>User role management</strong> so you can restrict access to Style Kits for your clients or editors', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:17
	// Reference: languages/ang-translations.php:1322
	__( 'Handy tools to <strong>highlight Elements</strong> using custom CSS or custom CSS classes', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:18
	// Reference: languages/ang-translations.php:1326
	__( '<strong>Tools to clean-up</strong> any of your templates and layouts from inline styles, and make them Style-Kit-ready', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:19
	// Reference: languages/ang-translations.php:1330
	__( '<strong>Unsplash integration</strong> so you can import images from Unsplash right into your WordPress media gallery', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:21
	// Reference: languages/ang-translations.php:1334
	__( 'And many more meaningful features', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings-gopro.php:22
	// Reference: languages/ang-translations.php:1338
	__( 'Explore Style Kits Pro', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:51
	// Reference: languages/ang-translations.php:1342
	__( 'Save changes', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:62
	// Reference: languages/ang-translations.php:1346
	__( 'Need help setting up? We have a number of handy articles to get you started.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:63
	// Reference: languages/ang-translations.php:1350
	__( 'Read Documentation', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:66
	// Reference: languages/ang-translations.php:1354
	__( 'Join our Facebook group', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:67
	// Reference: languages/ang-translations.php:1358
	__( 'Get insights, tips and updates in our facebook community. Let\'s take Elementor design to a whole new level.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:68
	// Reference: languages/ang-translations.php:1362
	__( 'Join the AnalogWP community', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:71
	// Reference: languages/ang-translations.php:1366
	__( 'Sign up for updates', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:72
	// Reference: languages/ang-translations.php:1370
	__( 'Sign up to Analog Newsletter and get notified about product updates, freebies and more.', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:75
	// Reference: languages/ang-translations.php:1374
	__( 'Sign me up', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:77
	// Reference: languages/ang-translations.php:1378
	__( 'By signing up you agree to our', 'ang' ),

	// Reference: inc/settings/views/html-admin-settings.php:77
	// Reference: languages/ang-translations.php:1382
	__( 'privacy and terms', 'ang' ),

	// Reference: inc/Utils.php:134
	// Reference: inc/Utils.php:640
	// Reference: languages/ang-translations.php:1389
	/* translators: translators: Global Style Kit post title.
translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. translators: translators: Global Style Kit post title. */
	__( 'Global: %s', 'ang' ),

	// Reference: languages/ang-translations.php:1393
	/* translators: translators: translators: translators: translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin translators: Description of the plugin */
	__( 'Style Kits adds intuitive styling controls in the Elementor editor that power-up your design workflow.', 'ang' ),

	// Reference: languages/ang-translations.php:1396
	__( 'Style Kits for Elementor Settings', 'ang' ),

	// Reference: languages/ang-translations.php:1400
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link */
	__( 'This setting has been moved to %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1403
	__( 'Style Kit settings', 'ang' ),

	// Reference: languages/ang-translations.php:1406
	__( 'Meet Style Kits by Style Kits for Elementor', 'ang' ),

	// Reference: languages/ang-translations.php:1409
	__( 'Tweak the size of your Heading elements using these presets.', 'ang' ),

	// Reference: languages/ang-translations.php:1412
	__( 'Tweak the size of text elements using these presets.', 'ang' ),

	// Reference: languages/ang-translations.php:1415
	__( 'As of now, Elementor doesn’t allow to edit Kits directly. You can only edit the kit Styles through the Theme Style panel.', 'ang' ),

	// Reference: languages/ang-translations.php:1418
	__( 'Imported Count', 'ang' ),

	// Reference: languages/ang-translations.php:1421
	__( 'Imported templates', 'ang' ),

	// Reference: languages/ang-translations.php:1424
	__( 'With the introduction of Theme Styles in Elementor v2.9.0, Style Kits are now <strong>integrated into the Theme Styles panel</strong>, bringing you a consistent, native experience.', 'ang' ),

	// Reference: languages/ang-translations.php:1427
	__( 'Read More about this', 'ang' ),

	// Reference: languages/ang-translations.php:1430
	__( 'Rollback to an earlier version', 'ang' ),

	// Reference: languages/ang-translations.php:1433
	__( 'Take me to SK Library', 'ang' ),

	// Reference: languages/ang-translations.php:1436
	__( 'Cheatin&#8217; huh?', 'ang' ),

	// Reference: languages/ang-translations.php:1439
	__( 'Style Kits requires Elementor v2.9.0 or greater in order to work. Please update Elementor to the latest version.', 'ang' ),

	// Reference: languages/ang-translations.php:1442
	__( 'Save Theme Style Kit as...', 'ang' ),

	// Reference: languages/ang-translations.php:1445
	__( 'Save as...', 'ang' ),

	// Reference: languages/ang-translations.php:1448
	__( 'Manage Style Kits', 'ang' ),

	// Reference: languages/ang-translations.php:1451
	__( 'Export Template', 'ang' ),

	// Reference: languages/ang-translations.php:1454
	__( 'Kit saved.', 'ang' ),

	// Reference: languages/ang-translations.php:1457
	__( 'This will reset all the settings you configured previously under Page Style Settings from Style Kits.', 'ang' ),

	// Reference: languages/ang-translations.php:1460
	__( 'Edit Style Kit in Theme Style', 'ang' ),

	// Reference: languages/ang-translations.php:1463
	__( 'Tools', 'ang' ),

	// Reference: languages/ang-translations.php:1466
	__( 'This will reset the Style Kit styles and detach the page from the Style Kit.', 'ang' ),

	// Reference: languages/ang-translations.php:1469
	__( 'Reset Style Kit', 'ang' ),

	// Reference: languages/ang-translations.php:1472
	__( 'Save all the styles as a Style Kit that you can apply on other pages or globally. Please note that only the custom styles added in the styles page are saved with the stylekit.', 'ang' ),

	// Reference: languages/ang-translations.php:1475
	__( 'Save Style Kit as...', 'ang' ),

	// Reference: languages/ang-translations.php:1478
	__( 'Export Style Kit CSS', 'ang' ),

	// Reference: languages/ang-translations.php:1481
	__( 'Analog Templates is not working because you need to activate the Elementor plugin.', 'ang' ),

	// Reference: languages/ang-translations.php:1484
	__( 'Analog Templates is not working because you need to install the Elementor plugin.', 'ang' ),

	// Reference: languages/ang-translations.php:1488
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: WordPress version translators: translators: translators: translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: translators: translators: translators: translators: translators: %s: WordPress version translators: translators: translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version translators: translators: %s: WordPress version */
	__( 'Analog Templates requires WordPress version %s+. Because you are using an earlier version, the plugin is currently NOT RUNNING.', 'ang' ),

	// Reference: languages/ang-translations.php:1491
	__( 'This will reset all the settings you configured previously under Page Style Settings from Analog Templates.', 'ang' ),

	// Reference: languages/ang-translations.php:1494
	__( 'This will reset all the custom style values added in the Style tab, and detach this page from any Style kits', 'ang' ),

	// Reference: languages/ang-translations.php:1497
	__( 'Reset all styling', 'ang' ),

	// Reference: languages/ang-translations.php:1500
	__( 'Reset all', 'ang' ),

	// Reference: languages/ang-translations.php:1503
	__( 'An inter-connected collection of Template Kits <br/>and advanced design control is <u>coming soon</u> with Style Kits Pro.', 'ang' ),

	// Reference: languages/ang-translations.php:1506
	__( 'You can <strong>sign up for an exclusive discount</strong> that we will send to your email when we are close to launch. Click the button below to see the Pro features and road map, and sign up for the exclusive discount.', 'ang' ),

	// Reference: languages/ang-translations.php:1509
	__( 'More about Style Kits Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1512
	__( 'Unable to create a post', 'ang' ),

	// Reference: languages/ang-translations.php:1515
	__( 'Token saved.', 'ang' ),

	// Reference: languages/ang-translations.php:1519
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. translators: translators: %1$s: Link to documentation, %2$s: Link text. */
	__( 'Set the colors for Typography, accents and more.', 'ang' ),

	// Reference: languages/ang-translations.php:1522
	__( 'The primary accent color applies on Links.', 'ang' ),

	// Reference: languages/ang-translations.php:1525
	__( 'The default Button color. You can also set button colors in the Buttons tab.', 'ang' ),

	// Reference: languages/ang-translations.php:1528
	__( 'Export Style Kit', 'ang' ),

	// Reference: languages/ang-translations.php:1531
	__( 'Import Style Kits', 'ang' ),

	// Reference: languages/ang-translations.php:1534
	__( 'Choose an Analog template JSON file or a .zip archive of Analog Style Kits, and add them to the list of Style Kits available in your library.', 'ang' ),

	// Reference: languages/ang-translations.php:1537
	__( 'Import Now', 'ang' ),

	// Reference: languages/ang-translations.php:1540
	__( 'Default Headings Font', 'ang' ),

	// Reference: languages/ang-translations.php:1543
	__( 'Buttons', 'ang' ),

	// Reference: languages/ang-translations.php:1546
	__( 'Text Hover Color', 'ang' ),

	// Reference: languages/ang-translations.php:1549
	__( 'Background Hover Color', 'ang' ),

	// Reference: languages/ang-translations.php:1552
	__( 'A style kit is a collection of all the custom styles added at page styling settings. Your Style Kit is updated every time you click the Update Style Kit Button below.', 'ang' ),

	// Reference: languages/ang-translations.php:1555
	__( 'Update Your Style Kit', 'ang' ),

	// Reference: languages/ang-translations.php:1559
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %1$s: Button Panel link/text. %2$s: Button sizes panel link/text. */
	__( 'The default button color. You can also define button colors under the buttons panel %1$s, and individually for each button size under %2$s.', 'ang' ),

	// Reference: languages/ang-translations.php:1562
	__( 'Sync Color Palettes and Style Kit colors by default', 'ang' ),

	// Reference: languages/ang-translations.php:1565
	__( 'The Elementor color palette will be populated with the Style Kit’s global colors', 'ang' ),

	// Reference: languages/ang-translations.php:1568
	__( 'Go PRO', 'ang' ),

	// Reference: languages/ang-translations.php:1571
	__( 'To import Pro templates, you’ll need an active ', 'ang' ),

	// Reference: languages/ang-translations.php:1574
	__( 'Subscribe up to newsletter', 'ang' ),

	// Reference: languages/ang-translations.php:1577
	__( 'Template Settings', 'ang' ),

	// Reference: languages/ang-translations.php:1580
	__( 'Remove Styling from typographic elements', 'ang' ),

	// Reference: languages/ang-translations.php:1583
	__( 'This setting will remove any values that have been manually added in the templates. Existing templates are not affected. ', 'ang' ),

	// Reference: languages/ang-translations.php:1586
	__( 'Export styles as custom CSS text.', 'ang' ),

	// Reference: languages/ang-translations.php:1589
	__( 'Export Custom CSS', 'ang' ),

	// Reference: languages/ang-translations.php:1592
	__( 'An inter-connected collection of Template Kits and advanced design control is <u>coming soon</u> with Style Kits Pro.', 'ang' ),

	// Reference: languages/ang-translations.php:1595
	__( 'Replace CSS selector with <code>selector</code> to make it easy to paste into custom CSS.', 'ang' ),

	// Reference: languages/ang-translations.php:1598
	__( 'Opt-in to our anonymous plugin data collection and to updates. We guarantee no sensitive data is collected.', 'ang' ),

	// Reference: languages/ang-translations.php:1601
	__( 'This setting will remove any values that have been manually added in the templates. Existing templates are not affected.', 'ang' ),

	// Reference: languages/ang-translations.php:1604
	__( 'Text and Headings Color', 'ang' ),

	// Reference: languages/ang-translations.php:1607
	__( 'Applies on the text and headings in the layout.', 'ang' ),

	// Reference: languages/ang-translations.php:1610
	__( 'Apply this color to sections or columns, using the <code>sk-light-bg</code>. The text inside will inherit the Text and titles color.', 'ang' ),

	// Reference: languages/ang-translations.php:1613
	__( 'Apply this color to sections or columns, using the <code>sk-dark-bg</code>. The text inside will inherit the <em>Text over Dark Background</em> color that can be set below.', 'ang' ),

	// Reference: languages/ang-translations.php:1616
	__( 'Text over dark background', 'ang' ),

	// Reference: languages/ang-translations.php:1619
	__( 'This color will apply on the text in a section or column with the Dark Background Color, as it has been set above.', 'ang' ),

	// Reference: languages/ang-translations.php:1622
	__( 'Template Kits are', 'ang' ),

	// Reference: languages/ang-translations.php:1625
	__( 'coming soon with Style Kits Pro.', 'ang' ),

	// Reference: languages/ang-translations.php:1628
	__( 'Sign up for an exclusive launch discount.', 'ang' ),

	// Reference: languages/ang-translations.php:1631
	__( 'The Block has been imported and is now available in the list of the available Sections.', 'ang' ),

	// Reference: languages/ang-translations.php:1634
	__( 'Rollback Version', 'ang' ),

	// Reference: languages/ang-translations.php:1637
	__( 'Theme License', 'ang' ),

	// Reference: languages/ang-translations.php:1640
	__( 'Enter your theme license key received upon purchase from <a target="_blank" href="https://analogwp.com/account/">AnalogWP</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1643
	__( 'License Key', 'ang' ),

	// Reference: languages/ang-translations.php:1646
	__( 'License Action', 'ang' ),

	// Reference: languages/ang-translations.php:1649
	__( 'Deactivate License', 'ang' ),

	// Reference: languages/ang-translations.php:1652
	__( 'Activate License', 'ang' ),

	// Reference: languages/ang-translations.php:1655
	__( 'License status is unknown.', 'ang' ),

	// Reference: languages/ang-translations.php:1658
	__( 'Renew?', 'ang' ),

	// Reference: languages/ang-translations.php:1661
	__( 'unlimited', 'ang' ),

	// Reference: languages/ang-translations.php:1664
	__( 'License key is active.', 'ang' ),

	// Reference: languages/ang-translations.php:1668
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date */
	__( 'Expires %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1671
	__( 'Lifetime License.', 'ang' ),

	// Reference: languages/ang-translations.php:1675
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit translators: translators: %1$s: active sites, %2$s: sites limit */
	__( 'You have %1$s / %2$s sites activated.', 'ang' ),

	// Reference: languages/ang-translations.php:1679
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name translators: translators: %s: product name */
	__( 'License key expired %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1682
	__( 'License key has expired.', 'ang' ),

	// Reference: languages/ang-translations.php:1685
	__( 'License keys do not match. <br> Enter your theme license key received upon purchase from <a target="_blank" href="https://analogwp.com/account/">AnalogWP</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1688
	__( 'License is inactive.', 'ang' ),

	// Reference: languages/ang-translations.php:1691
	__( 'License key is disabled.', 'ang' ),

	// Reference: languages/ang-translations.php:1694
	__( 'Site is inactive.', 'ang' ),

	// Reference: languages/ang-translations.php:1697
	__( 'Updating this theme will lose any customizations you have made. \'Cancel\' to stop, \'OK\' to update.', 'ang' ),

	// Reference: languages/ang-translations.php:1700
	__( '<strong>%1$s %2$s</strong> is available. <a href="%3$s" class="thickbox" title="%4$s">Check out what\'s new</a> or <a href="%5$s" %6$s>update now</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1703
	__( 'An error occurred, please try again.', 'ang' ),

	// Reference: languages/ang-translations.php:1707
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date translators: translators: %s: expiration date */
	__( 'Your license key expired on %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1710
	__( 'Your license key has been disabled.', 'ang' ),

	// Reference: languages/ang-translations.php:1713
	__( 'Invalid license.', 'ang' ),

	// Reference: languages/ang-translations.php:1716
	__( 'Your license is not active for this URL.', 'ang' ),

	// Reference: languages/ang-translations.php:1720
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email translators: translators: %s: site name/email */
	__( 'This appears to be an invalid license key for %s.', 'ang' ),

	// Reference: languages/ang-translations.php:1723
	__( 'Your license key has reached its activation limit.', 'ang' ),

	// Reference: languages/ang-translations.php:1726
	__( 'License', 'ang' ),

	// Reference: languages/ang-translations.php:1729
	__( 'License Status', 'ang' ),

	// Reference: languages/ang-translations.php:1732
	__( 'Pro license', 'ang' ),

	// Reference: languages/ang-translations.php:1735
	__( 'If you own an AnalogPro License, then please enter your license key here.', 'ang' ),

	// Reference: languages/ang-translations.php:1738
	__( 'If you do not have a license key, you can get one from ', 'ang' ),

	// Reference: languages/ang-translations.php:1741
	__( '<strong>You are editing the style kit that has been set as global.</strong> You can optionally choose a different Style Kit for this page below.', 'ang' ),

	// Reference: languages/ang-translations.php:1744
	__( 'Global Stylekit Settings Saved. It\'s recommended to close any open elementor tabs in your browser, and re-open them, for the effect to apply.', 'ang' ),

	// Reference: languages/ang-translations.php:1748
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link translators: translators: %s: Style Kit Documentation link */
	__( 'Choosing a Style Kit will make it global and apply site-wide. Learn more about <a href="%s" target="_blank">Style kits</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1751
	__( 'You will then be able to apply on any page.', 'ang' ),

	// Reference: languages/ang-translations.php:1754
	__( 'The Style Kit has been imported to your library.', 'ang' ),

	// Reference: languages/ang-translations.php:1757
	__( '%s has been imported and is now available in the Style Kits dropdown', 'ang' ),

	// Reference: languages/ang-translations.php:1760
	__( 'Do not apply link color on active titles', 'ang' ),

	// Reference: languages/ang-translations.php:1763
	__( 'Blimey! Your template has been imported.', 'ang' ),

	// Reference: languages/ang-translations.php:1766
	__( 'Premium template kits are coming soon with Style Kits Pro. You can only preview these layouts for now but feel free to sign up to our mailing list if you want to learn when they become available.', 'ang' ),

	// Reference: languages/ang-translations.php:1769
	__( 'View Kit', 'ang' ),

	// Reference: languages/ang-translations.php:1772
	__( 'Choose', 'ang' ),

	// Reference: languages/ang-translations.php:1775
	__( 'Global: ', 'ang' ),

	// Reference: languages/ang-translations.php:1778
	__( 'Access an interconnected library of Template Kits, blocks and additional design control with Style kits Pro.', 'ang' ),

	// Reference: languages/ang-translations.php:1781
	__( 'Template Kits with theme builder templates.', 'ang' ),

	// Reference: languages/ang-translations.php:1784
	__( 'Blocks Library.', 'ang' ),

	// Reference: languages/ang-translations.php:1787
	__( 'Global design control.', 'ang' ),

	// Reference: languages/ang-translations.php:1790
	__( 'Advanced Style Kit control.', 'ang' ),

	// Reference: languages/ang-translations.php:1793
	__( 'Back to library', 'ang' ),

	// Reference: languages/ang-translations.php:1796
	__( 'Elevate your Elementor design with Analog Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1799
	__( 'Step up your workflow with unlimited design resources for your Elementor-powered projects.', 'ang' ),

	// Reference: languages/ang-translations.php:1802
	__( 'Why Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1805
	__( 'Access to all template library', 'ang' ),

	// Reference: languages/ang-translations.php:1808
	__( 'Templates for singles, archives, popups and more', 'ang' ),

	// Reference: languages/ang-translations.php:1811
	__( 'Multi-niche, human made design that makes sense', 'ang' ),

	// Reference: languages/ang-translations.php:1814
	__( 'Unlimited license for peace of mind', 'ang' ),

	// Reference: languages/ang-translations.php:1817
	__( '* Requires Elementor Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1820
	__( 'Please update Analog Template plugin to latest version.', 'ang' ),

	// Reference: languages/ang-translations.php:1823
	__( 'Enter your email', 'ang' ),

	// Reference: languages/ang-translations.php:1826
	__( 'Sending...', 'ang' ),

	// Reference: languages/ang-translations.php:1829
	__( 'Sign up to newsletter', 'ang' ),

	// Reference: languages/ang-translations.php:1832
	__( 'Docs', 'ang' ),

	// Reference: languages/ang-translations.php:1835
	__( 'Follow on Social', 'ang' ),

	// Reference: languages/ang-translations.php:1838
	__( 'Elevate to Analog Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1841
	__( 'Do more with Analog Pro, the design library for complete Elementor-powered sites.', 'ang' ),

	// Reference: languages/ang-translations.php:1844
	__( 'Access to all templates', 'ang' ),

	// Reference: languages/ang-translations.php:1847
	__( 'New designs every week', 'ang' ),

	// Reference: languages/ang-translations.php:1850
	__( 'Flexible Licensing', 'ang' ),

	// Reference: languages/ang-translations.php:1853
	__( 'Pro Elements, theme builder layouts', 'ang' ),

	// Reference: languages/ang-translations.php:1856
	__( 'Requires Elementor Pro', 'ang' ),

	// Reference: languages/ang-translations.php:1859
	__( 'More Details', 'ang' ),

	// Reference: languages/ang-translations.php:1862
	__( 'An error occured', 'ang' ),

	// Reference: languages/ang-translations.php:1865
	__( 'Imported ', 'ang' ),

	// Reference: languages/ang-translations.php:1868
	__( 'Settings updated.', 'ang' ),

	// Reference: languages/ang-translations.php:1871
	__( 'Deactivate', 'ang' ),

	// Reference: languages/ang-translations.php:1874
	__( 'Activate', 'ang' ),

	// Reference: languages/ang-translations.php:1877
	__( 'Plugin Settings', 'ang' ),

	// Reference: languages/ang-translations.php:1880
	__( 'Pro License', 'ang' ),

	// Reference: languages/ang-translations.php:1883
	__( 'Connection timeout, please try again.', 'ang' ),

	// Reference: languages/ang-translations.php:1886
	__( 'Processing...', 'ang' ),

	// Reference: languages/ang-translations.php:1889
	__( 'If you do not have a license key, you can get one from', 'ang' ),

	// Reference: languages/ang-translations.php:1892
	__( 'Reinstall version %s', 'ang' ),

	// Reference: languages/ang-translations.php:1895
	__( 'Style Kits Settings Page', 'ang' ),

	// Reference: languages/ang-translations.php:1898
	__( 'License keys do not match. <br><br> Enter your theme license key received upon purchase from <a target="_blank" href="https://analogwp.com/account/">AnalogWP</a>.', 'ang' ),

	// Reference: languages/ang-translations.php:1901
	__( 'filter', 'ang' ),

	// Reference: languages/ang-translations.php:1904
	__( 'sort by', 'ang' ),

	// Reference: languages/ang-translations.php:1907
	__( 'show only free', 'ang' ),

	// Reference: languages/ang-translations.php:1911
	/* translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: translators: %s: Style kit title. */
	__( 'Style Kit: %s <span style="color:#3152FF;">&#9679;</span>', 'ang' ),

	// Reference: languages/ang-translations.php:1914
	__( 'This padding applies <strong>only on the Outer Sections</strong> of your layout, and not on Inner Section widgets.', 'ang' ),

	// Reference: languages/ang-translations.php:1917
	__( 'Set the default values of the column gaps. Based on Elementor&apos;s default sizes.', 'ang' ),

	// Reference: languages/ang-translations.php:1920
	__( 'Create the styles for each button size', 'ang' ),

	// Reference: languages/ang-translations.php:1923
	__( 'View Library', 'ang' ),

	// Reference: languages/ang-translations.php:1926
	__( 'Kits', 'ang' ),

	// Reference: languages/ang-translations.php:1929
	__( 'Below are the available Style Kits. When you choose to import a Style Kit, it will be added to your available', 'ang' ),

	// Reference: languages/ang-translations.php:1932
	__( 'Importing Style Kit ', 'ang' ),

	// Reference: languages/ang-translations.php:1935
	__( 'Blimey! Your Style Kit has been imported to library.', 'ang' ),

	// Reference: languages/ang-translations.php:1938
	__( 'Normal Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1941
	__( 'Small Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1944
	__( 'Medium Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1947
	__( 'Large Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1950
	__( 'Extra Large Padding', 'ang' ),

	// Reference: languages/ang-translations.php:1953
	__( 'No Gap', 'ang' ),

	// Reference: languages/ang-translations.php:1956
	__( 'Sets the primary brand color, applies on Links.', 'ang' ),

	// Reference: languages/ang-translations.php:1959
	__( 'Sets the default color for Buttons. You can also apply button colors in the Global buttons tab.', 'ang' ),

	// Reference: languages/ang-translations.php:1962
	__( 'Text over light background', 'ang' ),

	// Reference: languages/ang-translations.php:1965
	__( 'Applies on the text and titles over a section or column with light bg', 'ang' ),

	// Reference: languages/ang-translations.php:1968
	__( 'Applies on the text and titles over a section or column with dark bg', 'ang' ),

	// Reference: languages/ang-translations.php:1971
	__( 'Apply the class <strong>sk-light-bg</strong> to a section or column to apply this color as a background. Text will inherit the <strong>Text over Light bg</strong> color.', 'ang' ),

	// Reference: languages/ang-translations.php:1974
	__( 'Apply the class <strong>sk-dark-bg</strong> to a section or column to apply this color as a background. Text will inherit the <strong>Text over Dark bg</strong> color.', 'ang' ),

	// Reference: languages/ang-translations.php:1977
	__( 'Narrow', 'ang' ),

	// Reference: languages/ang-translations.php:1980
	__( 'Extended', 'ang' ),

	// Reference: languages/ang-translations.php:1983
	__( 'Wide', 'ang' ),

	// Reference: languages/ang-translations.php:1986
	__( 'Wider', 'ang' ),

	// Reference: languages/ang-translations.php:1989
	__( 'Outer Section Gaps', 'ang' ),

	// Reference: languages/ang-translations.php:1992
	__( 'A handcrafted design library for Elementor templates.', 'ang' ),

	// Reference: languages/ang-translations.php:1995
	__( 'Outer Section Gap', 'ang' ),

	// Reference: languages/ang-translations.php:1998
	__( 'Set the padding <strong>only for the outer section</strong>, does not apply on inner section widgets, in case you have any. You can tweak the section gap', 'ang' ),

	// Reference: languages/ang-translations.php:2001
	__( 'Set the padding <strong>only for the outer section</strong>, does not apply on inner section widgets, in case you have any. You can tweak the section gap here.', 'ang' ),

	// Reference: languages/ang-translations.php:2004
	__( 'AnalogWP Templates', 'ang' ),

	// Reference: languages/ang-translations.php:2007
	__( 'Analog Templates', 'ang' ),

	// Reference: languages/ang-translations.php:2010
	__( 'AnalogWP Settings', 'ang' ),

	// Reference: languages/ang-translations.php:2013
	__( 'Meet Style Kits by AnalogWP', 'ang' ),

	// Reference: languages/ang-translations.php:2016
	__( 'Page Styles', 'ang' ),

	// Reference: languages/ang-translations.php:2019
	__( 'Font Size', 'ang' ),

	// Reference: languages/ang-translations.php:2022
	__( 'Line Height', 'ang' ),

	// Reference: languages/ang-translations.php:2025
	__( 'AnalogWP Shortcuts', 'ang' ),

	// Reference: languages/ang-translations.php:2028
	__( 'Applies to all links by default', 'ang' ),

	// Reference: languages/ang-translations.php:2031
	__( 'Adds a background color to all buttons on page.', 'ang' ),

	// Reference: languages/ang-translations.php:2034
	__( 'Text and Headings (Dark)', 'ang' ),

	// Reference: languages/ang-translations.php:2037
	__( 'Applies on the text and titles over light bg', 'ang' ),

	// Reference: languages/ang-translations.php:2040
	__( 'Text and Headings (Light)', 'ang' ),

	// Reference: languages/ang-translations.php:2043
	__( 'Applies on the text and titles over dark bg', 'ang' ),

	// Reference: languages/ang-translations.php:2046
	__( 'If you are having issues with current version of Analog Templates, you can rollback to a previous stable version.', 'ang' ),

	// Reference: inc/elementor/class-colors.php:153
	// Reference: languages/ang-translations.php:2050
	_x( 'Accent Colors', 'Section Title', 'ang' ),

	// Reference: inc/elementor/Promotions.php:48
	// Reference: languages/ang-translations.php:2054
	_x( 'Layout Tools', 'Section Title', 'ang' ),

	// Reference: inc/elementor/Promotions.php:85
	// Reference: languages/ang-translations.php:2058
	_x( 'Forms (Extended)', 'Section Title', 'ang' ),

	// Reference: inc/elementor/sections/background-color-classes.php:51
	// Reference: languages/ang-translations.php:2062
	_x( 'Background Color Classes', 'Section Title', 'ang' ),

	// Reference: languages/ang-translations.php:2065
	_x( 'Main Colors', 'Section Title', 'ang' ),

	// Reference: languages/ang-translations.php:2068
	_x( 'Background Colors', 'Section Title', 'ang' ),

	// Reference: languages/ang-translations.php:2071
	_x( 'Global Colors', 'Section Title', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:34
	// Reference: languages/ang-translations.php:2075
	_x( 'Style Kits', 'admin menu', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:35
	// Reference: languages/ang-translations.php:2079
	_x( 'Style Kit', 'add new on admin bar', 'ang' ),

	// Reference: inc/elementor/class-post-type.php:36
	// Reference: languages/ang-translations.php:2083
	_x( 'Add New', 'book', 'ang' ),

	// Reference: inc/elementor/kit/Kits_List_Table.php:265
	// Reference: languages/ang-translations.php:2089
	/* translators: translators: %s: Number of posts.
translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. translators: translators: %s: Number of posts. */
	_nx_noop( 'All <span class="count">(%s)</span>',  'All <span class="count">(%s)</span>', 'posts', 'ang' ),

	// Reference: inc/settings/class-settings-general.php:66
	// Reference: languages/ang-translations.php:2093
	_x( 'Global Style Kit', 'settings title', 'ang' ),

	// Reference: languages/ang-translations.php:2096
	_x( 'Sync Color Palettes', 'settings title', 'ang' )
);
/* THIS IS THE END OF THE GENERATED FILE */
